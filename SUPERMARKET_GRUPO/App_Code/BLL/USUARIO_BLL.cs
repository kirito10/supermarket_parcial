﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de USUARIO_BLL
/// </summary>
public class USUARIO_BLL
{
    public USUARIO_BLL()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public static List<USUARIO_DTO> MostrarTodos()
    {
        List<USUARIO_DTO> mostraroles = new List<USUARIO_DTO>();
        MARKET_DSTableAdapters.USUARIOTableAdapter adto = new MARKET_DSTableAdapters.USUARIOTableAdapter();
        MARKET_DS.USUARIODataTable tabla = adto.MostrarUsuario();
        foreach (MARKET_DS.USUARIORow fila in tabla)
        {
            mostraroles.Add(filaSDTO(fila));
        }
        return mostraroles;
    }

    private static USUARIO_DTO filaSDTO(MARKET_DS.USUARIORow fila)
    {
        USUARIO_DTO objUSUARIO = new USUARIO_DTO();
        //string cont = (fila.CONTRASENA_USUARIO).ToString();

        objUSUARIO.ID_USUARIO = fila.ID_USUARIO;
        objUSUARIO.NOMBRE_USUARIO = fila.NOMBRE_USUARIO;
        //objUSUARIO.CONTRASENA_USUARIO = cont;
        objUSUARIO.CONTRASENA_USUARIO = fila.CONTRASENA_USUARIO;
        objUSUARIO.ID_TIPO_USUARIO = fila.ID_TIPO_USUARIO;
        objUSUARIO.ID_PERSONA_USUARIO = fila.ID_PERSONA_USUARIO;
        return objUSUARIO;
    }

    public static USUARIO_DTO MostrarID(int ID_USUARIO)
    {
        MARKET_DSTableAdapters.USUARIOTableAdapter adto = new MARKET_DSTableAdapters.USUARIOTableAdapter();
        MARKET_DS.USUARIODataTable tabla = adto.MostrarUsuarioID(ID_USUARIO);
        if (tabla.Rows.Count == 0)
        {
            return null;
        }
        return filaSDTO(tabla[0]);
    }

    public static void Insertar(int ID_USUARIO, string NOMBRE_USUARIO, string CONTRASENA_USUARIO, int ID_TIPO_USUARIO, int ID_PERSONA_USUARIO)
    {
        MARKET_DSTableAdapters.USUARIOTableAdapter adto = new MARKET_DSTableAdapters.USUARIOTableAdapter();
        adto.Insert(ID_USUARIO, CONTRASENA_USUARIO, NOMBRE_USUARIO, ID_TIPO_USUARIO, ID_PERSONA_USUARIO);
    }

    public static void Borrar(int ID_USUARIO)
    {
        MARKET_DSTableAdapters.USUARIOTableAdapter adto = new MARKET_DSTableAdapters.USUARIOTableAdapter();
        adto.Delete(ID_USUARIO);
    }

    public static void Actualizar(int ID_USUARIO, string NOMBRE_USUARIO, string CONTRASENA_USUARIO, int ID_TIPO_USUARIO, int ID_PERSONA_USUARIO)
    {
        MARKET_DSTableAdapters.USUARIOTableAdapter adto = new MARKET_DSTableAdapters.USUARIOTableAdapter();
        adto.Update(NOMBRE_USUARIO, CONTRASENA_USUARIO, ID_TIPO_USUARIO, ID_PERSONA_USUARIO, ID_USUARIO);
    }
}