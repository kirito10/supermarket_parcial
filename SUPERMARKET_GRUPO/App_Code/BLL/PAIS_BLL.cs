﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PAIS_DTO
/// </summary>
public class PAIS_BILL
{
    public PAIS_BILL()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public static List<PAIS_DTO> MostrarTodos()
    {
        List<PAIS_DTO> mostraroles = new List<PAIS_DTO>();
        MARKET_DSTableAdapters.PAISTableAdapter adto = new MARKET_DSTableAdapters.PAISTableAdapter();
        MARKET_DS.PAISDataTable tabla = adto.MostrarPais();
        foreach (MARKET_DS.PAISRow fila in tabla)
        {
            mostraroles.Add(filaSDTO(fila));
        }
        return mostraroles;
    }

    private static PAIS_DTO filaSDTO(MARKET_DS.PAISRow fila)
    {
        PAIS_DTO objPAIS = new PAIS_DTO();
        objPAIS.ID_PAIS = fila.ID_PAIS;
        objPAIS.NOMBRE_PAIS = fila.NOMBRE_PAIS;
        return objPAIS;
    }

    public static PAIS_DTO MostrarID(int ID_PAIS)
    {
        MARKET_DSTableAdapters.PAISTableAdapter adto = new MARKET_DSTableAdapters.PAISTableAdapter();
        MARKET_DS.PAISDataTable tabla = adto.MostrarPaisID(ID_PAIS);
        if (tabla.Rows.Count == 0)
        {
            return null;
        }
        return filaSDTO(tabla[0]);
    }

    public static void Insertar(int ID_PAIS, string NOMB)
    {
        MARKET_DSTableAdapters.PAISTableAdapter adto = new MARKET_DSTableAdapters.PAISTableAdapter();
        adto.Insert(ID_PAIS, NOMB);
    }

    public static void Borrar(int ID_PAIS)
    {
        MARKET_DSTableAdapters.PAISTableAdapter adto = new MARKET_DSTableAdapters.PAISTableAdapter();
        adto.Delete(ID_PAIS);
    }

    public static void Actualizar(int ID_PAIS, string NOMB)
    {
        MARKET_DSTableAdapters.PAISTableAdapter adto = new MARKET_DSTableAdapters.PAISTableAdapter();
        adto.Update(NOMB, ID_PAIS );
    }
}