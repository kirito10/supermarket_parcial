﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de SEXO_BLL
/// </summary>
public class SEXO_BLL
{
    public SEXO_BLL()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    

    public static List<SEXO_DTO> MostrarTodos()
    {
        List<SEXO_DTO> mostrar = new List<SEXO_DTO>();
        MARKET_DSTableAdapters.SEXOTableAdapter adto = new MARKET_DSTableAdapters.SEXOTableAdapter();
        MARKET_DS.SEXODataTable tabla = adto.MostrarSexo();
        foreach (MARKET_DS.SEXORow fila in tabla)
        {
            mostrar.Add(filaSDTO(fila));
        }
        return mostrar;
    }

    private static SEXO_DTO filaSDTO(MARKET_DS.SEXORow fila)
    {
        SEXO_DTO objSEXO = new SEXO_DTO();

        objSEXO.ID_SEXO = fila.ID_SEXO;
        objSEXO.SEXO = fila.SEXO;
        return objSEXO;
    }

    public static SEXO_DTO MostrarID(int ID_SEXO)
    {
        MARKET_DSTableAdapters.SEXOTableAdapter adto = new MARKET_DSTableAdapters.SEXOTableAdapter();
        MARKET_DS.SEXODataTable tabla = adto.MostrarSexoID(ID_SEXO);
        if (tabla.Rows.Count == 0)
        {
            return null;
        }
        return filaSDTO(tabla[0]);
    }

    public static void Insertar(int ID_SEXO, string SEXO)
    {
        MARKET_DSTableAdapters.SEXOTableAdapter adto = new MARKET_DSTableAdapters.SEXOTableAdapter();
        adto.Insert(ID_SEXO, SEXO);
    }

    public static void Borrar(int ID_SEXO)
    {
        MARKET_DSTableAdapters.SEXOTableAdapter adto = new MARKET_DSTableAdapters.SEXOTableAdapter();
        adto.Delete(ID_SEXO);
    }

    public static void Actualizar(int ID_SEXO, string SEXO)
    {
        MARKET_DSTableAdapters.SEXOTableAdapter adto = new MARKET_DSTableAdapters.SEXOTableAdapter();
        adto.Update(SEXO, ID_SEXO);
    }
}