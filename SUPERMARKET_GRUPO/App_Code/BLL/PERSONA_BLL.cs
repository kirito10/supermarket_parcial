﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PERSONA_BLL
/// </summary>
public class PERSONA_BLL
{
    public PERSONA_BLL()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public static List<PERSONA_DTO> MostrarTodos()
    {
        List<PERSONA_DTO> mostrar = new List<PERSONA_DTO>();
        MARKET_DSTableAdapters.PERSONATableAdapter adto = new MARKET_DSTableAdapters.PERSONATableAdapter();
        MARKET_DS.PERSONADataTable tabla = adto.MostrarPersona();
        foreach (MARKET_DS.PERSONARow fila in tabla)
        {
            mostrar.Add(filaSDTO(fila));
        }
        return mostrar;
    }

    private static PERSONA_DTO filaSDTO(MARKET_DS.PERSONARow fila)
    {
        PERSONA_DTO objPERSONA = new PERSONA_DTO();

        objPERSONA.ID_PERSONA = fila.ID_PERSONA;
        objPERSONA.PRIMER_NOMBRE_PERSONA = fila.PRIMER_NOMBRE_PERSONA;
        objPERSONA.SEGUNDO_NOMBRE_PERSONA = fila.SEGUNDO_NOMBRE_PERSONA;
        objPERSONA.PRIMER_APELLIDO_PERSONA = fila.PRIMER_APELLIDO_PERSONA;
        objPERSONA.SEGUNDO_APELLIDO_PERSONA = fila.SEGUNDO_APELLIDO_PERSONA;
        objPERSONA.FECHA_NACIMIENTO_PERSONA = fila.FECHA_NACIMIENTO_PERSONA;
        objPERSONA.NUMERO_IDENTIDAD_PERSONA = fila.NUMERO_IDENTIDAD_PERSONA;
        objPERSONA.DIRECCION_RESIDENCIA_PERSONA = fila.DIRECCION_RESIDENCIA_PERSONA;
        objPERSONA.ID_CIUDAD_RESIDENCIA_PERSONA = fila.ID_CIUDAD_RESIDENCIA_PERSONA;
        objPERSONA.ID_SEXO_PERSONA = fila.ID_SEXO_PERSONA;
        return objPERSONA;
    }

    public static PERSONA_DTO MostrarID(int ID_PERSONA)
    {
        MARKET_DSTableAdapters.PERSONATableAdapter adto = new MARKET_DSTableAdapters.PERSONATableAdapter();
        MARKET_DS.PERSONADataTable tabla = adto.MostrarPersonaID(ID_PERSONA);
        if (tabla.Rows.Count == 0)
        {
            return null;
        }
        return filaSDTO(tabla[0]);
    }

    public static void Insertar(int ID_PERSONA, string PRIMER_NOMBRE_PERSONA, string SEGUNDO_NOMBRE_PERSONA, string PRIMER_APELLIDO_PERSONA, string SEGUNDO_APELLIDO_PERSONA, DateTime FECHA_NACIMIENTO_PERSONA, string NUMERO_IDENTIDAD_PERSONA, string DIRECCION_RESIDENCIA_PERSONA, int ID_CIUDAD_RESIDENCIA_PERSONA, int ID_SEXO_PERSONA)
    {
        MARKET_DSTableAdapters.PERSONATableAdapter adto = new MARKET_DSTableAdapters.PERSONATableAdapter();
        adto.Insert(ID_PERSONA, PRIMER_NOMBRE_PERSONA, SEGUNDO_NOMBRE_PERSONA, PRIMER_APELLIDO_PERSONA, SEGUNDO_APELLIDO_PERSONA, FECHA_NACIMIENTO_PERSONA, NUMERO_IDENTIDAD_PERSONA, DIRECCION_RESIDENCIA_PERSONA, ID_CIUDAD_RESIDENCIA_PERSONA, ID_SEXO_PERSONA);
    }

    public static void Borrar(int ID_PERSONA)
    {
        MARKET_DSTableAdapters.PERSONATableAdapter adto = new MARKET_DSTableAdapters.PERSONATableAdapter();
        adto.Delete(ID_PERSONA);
    }

    public static void Actualizar(int ID_PERSONA, string PRIMER_NOMBRE_PERSONA, string SEGUNDO_NOMBRE_PERSONA, string PRIMER_APELLIDO_PERSONA, string SEGUNDO_APELLIDO_PERSONA, DateTime FECHA_NACIMIENTO_PERSONA, string NUMERO_IDENTIDAD_PERSONA, string DIRECCION_RESIDENCIA_PERSONA, int ID_CIUDAD_RESIDENCIA_PERSONA, int ID_SEXO_PERSONA)
    {
        MARKET_DSTableAdapters.PERSONATableAdapter adto = new MARKET_DSTableAdapters.PERSONATableAdapter();
        adto.Update(PRIMER_NOMBRE_PERSONA, SEGUNDO_NOMBRE_PERSONA, PRIMER_APELLIDO_PERSONA, SEGUNDO_APELLIDO_PERSONA, FECHA_NACIMIENTO_PERSONA, NUMERO_IDENTIDAD_PERSONA, DIRECCION_RESIDENCIA_PERSONA, ID_CIUDAD_RESIDENCIA_PERSONA, ID_SEXO_PERSONA, ID_PERSONA);
    }

}