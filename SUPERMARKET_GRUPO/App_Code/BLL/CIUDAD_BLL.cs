﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de CIUDAD_BLL
/// </summary>
public class CIUDAD_BLL
{
    public CIUDAD_BLL()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public static List<CIUDAD_DTO> MostrarTodos()
    {
        List<CIUDAD_DTO> mostrar = new List<CIUDAD_DTO>();
        MARKET_DSTableAdapters.CIUDADTableAdapter adto = new MARKET_DSTableAdapters.CIUDADTableAdapter();
        MARKET_DS.CIUDADDataTable tabla = adto.MostrarCiudad();
        foreach (MARKET_DS.CIUDADRow fila in tabla)
        {
            mostrar.Add(filaSDTO(fila));
        }
        return mostrar;
    }

    private static CIUDAD_DTO filaSDTO(MARKET_DS.CIUDADRow fila)
    {
        CIUDAD_DTO objCIUDAD = new CIUDAD_DTO();

        objCIUDAD.ID_CIUDAD = fila.ID_CIUDAD;
        objCIUDAD.NOMBRE_CIUDAD = fila.NOMBRE_CIUDAD;
        objCIUDAD.ID_DEPARTAMENTO_CIUDAD = fila.ID_DEPARTAMENTO_CIUDAD;
        return objCIUDAD;
    }

    public static CIUDAD_DTO MostrarID(int ID_CIUDAD)
    {
        MARKET_DSTableAdapters.CIUDADTableAdapter adto = new MARKET_DSTableAdapters.CIUDADTableAdapter();
        MARKET_DS.CIUDADDataTable tabla = adto.MostrarCiudadID(ID_CIUDAD);
        if (tabla.Rows.Count == 0)
        {
            return null;
        }
        return filaSDTO(tabla[0]);
    }

    public static void Insertar(int ID_CIUDAD, string NOMBRE_CIUDAD, int ID_DEPARTAMENTO_CIUDAD)
    {
        MARKET_DSTableAdapters.CIUDADTableAdapter adto = new MARKET_DSTableAdapters.CIUDADTableAdapter();
        adto.Insert(ID_CIUDAD, NOMBRE_CIUDAD, ID_DEPARTAMENTO_CIUDAD);
    }

    public static void Borrar(int ID_CIUDAD)
    {
        MARKET_DSTableAdapters.CIUDADTableAdapter adto = new MARKET_DSTableAdapters.CIUDADTableAdapter();
        adto.Delete(ID_CIUDAD);
    }

    public static void Actualizar(int ID_CIUDAD, string NOMBRE_CIUDAD, int ID_DEPARTAMENTO_CIUDAD)
    {
        MARKET_DSTableAdapters.CIUDADTableAdapter adto = new MARKET_DSTableAdapters.CIUDADTableAdapter();
        adto.Update(NOMBRE_CIUDAD, ID_CIUDAD, ID_DEPARTAMENTO_CIUDAD);
    }
}