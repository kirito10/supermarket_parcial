﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de DEPARTAMENTO_BLL
/// </summary>
public class DEPARTAMENTO_BLL
{
    public DEPARTAMENTO_BLL()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    public static List<DEPARTAMENTO_DTO> MostrarTodos()
    {
        List<DEPARTAMENTO_DTO> mostrar = new List<DEPARTAMENTO_DTO>();
        MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter adto = new MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter();
        MARKET_DS.DEPARTAMENTODataTable tabla = adto.MostrarDepartamento();
        foreach (MARKET_DS.DEPARTAMENTORow fila in tabla)
        {
            mostrar.Add(filaSDTO(fila));
        }
        return mostrar;
    }

    private static DEPARTAMENTO_DTO filaSDTO(MARKET_DS.DEPARTAMENTORow fila)
    {
        DEPARTAMENTO_DTO objDEPART = new DEPARTAMENTO_DTO();

        objDEPART.ID_DEPARTAMENTO = fila.ID_DEPARTAMENTO;
        objDEPART.NOMBRE_DEPARTAMENTO = fila.NOMBRE_DEPARTAMENTO;
        objDEPART.ID_PAIS_DEPARTAMENTO = fila.ID_PAIS_DEPARTAMENTO;
        return objDEPART;
    }

    public static DEPARTAMENTO_DTO MostrarID(int ID_CIUDAD)
    {
        MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter adto = new MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter();
        MARKET_DS.DEPARTAMENTODataTable tabla = adto.MostarDepartamentoID(ID_CIUDAD);
        if (tabla.Rows.Count == 0)
        {
            return null;
        }
        return filaSDTO(tabla[0]);
    }

    public static void Insertar(int ID_DEPARTAMENTO, string NOMBRE_DEPARTAMENTO, int ID_PAIS_DEPARTAMENTO)
    {
        MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter adto = new MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter();
        adto.Insert(ID_DEPARTAMENTO, NOMBRE_DEPARTAMENTO, ID_PAIS_DEPARTAMENTO);
    }

    public static void Borrar(int ID_DEPARTAMENTO)
    {
        MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter adto = new MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter();
        adto.Delete(ID_DEPARTAMENTO);
    }

    public static void Actualizar(int ID_DEPARTAMENTO, string NOMBRE_DEPARTAMENTO, int ID_PAIS_DEPARTAMENTO)
    {
        MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter adto = new MARKET_DSTableAdapters.DEPARTAMENTOTableAdapter();
        adto.Update(NOMBRE_DEPARTAMENTO, ID_DEPARTAMENTO, ID_PAIS_DEPARTAMENTO);
    }
}