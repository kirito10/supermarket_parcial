﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webFormCli_Registro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        int Iduser = Convert.ToInt32(txtUserId.Text);
        string USER = txtUsuario.Text;
        string pass = Encriptar.Encrip(txtPassword.Text);
        try
        {
            USUARIO_BLL.Insertar(Iduser, USER, pass);
            Response.Redirect("~/MostrarRoles.aspx");
        }
        catch (Exception)
        {
            
        }
    }


    protected void btnActualizar_Click(object sender, EventArgs e)
    {
        int idRol = Convert.ToInt32(txtUserId.Text);
        string nombrRol = txtUsuario.Text;
        string PASS = Encriptar.Encrip(txtPassword.Text);
        try
        {
            USUARIO_BLL.Actualizar(idRol, nombrRol, PASS);
            Response.Redirect("~/MostrarRoles.aspx");
        }
        catch (Exception)
        {
            
        }
    }
}